
// interface Menu {
//     name: string
//     subMenu: SubMenu[]
// }

// interface SubMenu {
//     name: string
// }

// const menus: Menu[] = [
//     {
//         name: 'Home',
//         subMenu: [],
//     },
//     {
//         name: 'About',
//         subMenu: [
//             {
//                 name: 'Company',
//             },
//             {
//                 name: 'Team',
//             },
//         ],
//     },
//     {
//         name: 'Products',
//         subMenu: [
//             {
//                 name: 'Electronics',
//             },
//             {
//                 name: 'Clothing',
//             },
//             {
//                 name: 'Accessories',
//             },
//         ],
//     },
//     {
//         name: 'Services',
//         subMenu: [],
//     },
//     {
//         name: 'Contact',
//         subMenu: [
//             {
//                 name: 'Phone',
//             },
//         ],
//     },
//     {
//         name: 'Blog',
//         subMenu: [],
//     },
//     {
//         name: 'Gallery',
//         subMenu: [
//             {
//                 name: 'Photos',
//             },
//             {
//                 name: 'Videos',
//             },
//             {
//                 name: 'Events',
//             },
//         ],
//     },
//     {
//         name: 'FAQ',
//         subMenu: [],
//     },
//     {
//         name: 'Downloads',
//         subMenu: [
//             {
//                 name: 'Documents',
//             },
//             {
//                 name: 'Software',
//             },
//         ],
//     },
//     {
//         name: 'Support',
//         subMenu: [
//             {
//                 name: 'Help Center',
//             },
//             {
//                 name: 'Contact Us',
//             },
//             {
//                 name: 'Knowledge Base',
//             },
//         ],
//     },
// ];

// const div = document.getElementById('ex1') as HTMLDivElement
// const ul = document.createElement('ul')
// for (const menu of menus) {
//     const li = document.createElement('li')
//     const name = document.createTextNode(menu.name)
//     li.appendChild(name)
//     if (menu.subMenu.length > 0) {
//         const ulSub = document.createElement('ul')
//         for (const submenu of menu.subMenu) {
//             const liSub = document.createElement('li')
//             const nameSub = document.createTextNode(submenu.name)
//             liSub.appendChild(nameSub)
//             ulSub.appendChild(liSub)
//         }
//         li.appendChild(ulSub)
//     }
//     ul.appendChild(li)

// }
// div.appendChild(ul)


interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];

const div2 = document.getElementById('ex2') as HTMLDivElement
const ul2 = document.createElement('ul')
for (const question of questions) {
    const li2 = document.createElement('p')
    const name2 = document.createTextNode(question.question)
    li2.appendChild(name2)
    const ulSub2 = document.createElement('ul')
    for (const choice of question.choices) {
        const liSub2 = document.createElement('li')
        const nameSub2 = document.createTextNode(choice)
        liSub2.appendChild(nameSub2)
        ulSub2.appendChild(liSub2)
    }
    li2.appendChild(ulSub2)
    ul2.appendChild(li2)
}
div2.appendChild(ul2)



//div, ul, 
export { questions, div2, ul2   }




