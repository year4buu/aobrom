const inputNumber = document.getElementById('inputnumber')
const plusbtn = document.getElementById('plusbtn')
const minbtn = document.getElementById('minbtn')
const mulbtn = document.getElementById('mulbtn')
const divbtn = document.getElementById('divbtn')
const output = document.getElementById('output')
const equalbtn = document.getElementById('equalbtn')
const notequalbtn = document.getElementById('notequalbtn')
const greater = document.getElementById('greater')
const greaterthanorequal = document.getElementById('greaterthanorequal')
plusbtn.addEventListener('click', function () {
    const value = inputNumber.value
    const result = parseInt(value) + 2

    inputNumber.innerText = result
    output.innerText = result
})
minbtn.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value - 2
    inputNumber.innerText = result
    output.innerText = result
})
mulbtn.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value * 2
    inputNumber.innerText = result
    output.innerText = result
})
divbtn.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value / 2
    inputNumber.innerText = result
    output.innerText = result
})
     
equalbtn.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value == 10

    output.innerText = result
})
notequalbtn.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value != 10

    output.innerText = result
})
greater.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value > 10

    output.innerText = result
})
greaterthanorequal.addEventListener('click', function () {
    const value = inputNumber.value
    const result = value >= 10

    output.innerText = result
})